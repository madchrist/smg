Simple Media Gallery
====================

Creates a gallery of the current directory and its sub-directories

Demo
----
[Here](http://madx.org/projects/smg/demo)

Features
--------
* Supports: gif, jpeg, png, wbmp, xbm, xpm, svg, mp4, ogg and webm
* Automatic thumbnail creation
* A picture's date, time, latitude, longitude and altitude are displayed if present. Clicking the lat/lon redirects to google maps
* Files and directories starting with a dot are ignored
* Made of HTML5, CSS and SVGs. No flash, no javascript. Browser compatibility: any modern browser will do
* Everything is contained in one file

Requirements
------------
* PHP 5.4+ with gd, exif and zlib support
* If gd support is missing, pictures will have a generic thumbnail
* If exif support is missing, picture infos (date, gps, etc) will not be shown

Install
-------
* Copy the script (index.php) into the directory you want to create a gallery of
* The script must be able to create and write in ".smg_cache"
* There are a few configuration variables at the top of the script, default values should fit most people

Uninstall
---------
* Remove the script and ".smg_cache"


