<?php
/*
 * SMG (Simple Media Gallery)
 */

/*****************
 * CONFIGURATION *
 *****************/
ini_set('memory_limit', '256M'); // needed to create thumbnails of large images (from DSLRs for example), increase this if some thumbnails do not appear
$cache_dir = '.smg_cache';       // directory where all the thumbnails are stored, must be writeable. It *must* start with a dot, otherwise thumbnails of thumbnails would be created
$sizes = array('small'    => array('width' => 128, 'height' => 96 ),  // Sizes of thumbnails. Aspect is always preserved and pictures are never upscaled
               'large'   => array('width' => 800, 'height' => 600));
//error_reporting(E_ERROR); ini_set('display_errors', '1'); header("Cache-Control: max-age=1"); // debug mode

/*******
 * CSS *
 *******/
$css = '* { margin:0; padding:0; border-style:none; color:#D0D0D0; text-shadow:0.1em 0.1em 0.1em black; outline:none; vertical-align:middle; text-align:center; }' .
       'html { background-color: #222222; }' .
       'html, body { height:100%; width:100%; }' .
       '.link { background-color:#333333; } .link:hover { background-color:#444444; } .link:active { background-color:#555555; }' .
       '.ui { display:block; position:fixed; z-index:9999; float:none; width:8em; box-shadow:0 0 .8em .2em black; } ' .
       '.up { top:0; left:0; border-radius:0 0 .6em 0; }' .
       '.prev { top:50%; left:0; border-radius:0 .6em .6em 0; }' .
       '.next { top:50%; right:0; border-radius:.6em 0 0 .6em; }' .
       '.wrapper { display:table; height:100%; width:100%; } .wrapped { display:table-cell; }' .
       '.gallery a { display:inline-block; margin:.4em; padding:.6em; text-decoration:none; box-shadow:.3em .3em .4em black; border-radius:.6em; }' .
       '.gallery .thumbnail_small { width: ' . $sizes['small']['width'] . 'px; height:' . $sizes['small']['height'] . 'px; } ' .
       '.gallery .thumbnail_small img { border-radius:.3em; max-width: ' . $sizes['small']['width'] . 'px; max-height:' . $sizes['small']['height'] . 'px; }' .
       'video { width:80%; height:auto; box-shadow:0 0 2em black; border-radius:.8em; }' .
       '.video_download { margin:auto; font-size:2em; } ' .
       'img.thumbnail_large { box-shadow:0 0 2em black; border-radius:.8em; max-width: ' . $sizes['large']['width'] . 'px; max-height:' . $sizes['large']['height'] . 'px; } ' .
       '.exif { margin:auto; } .exif td, .exif td a:link, .exif td a:visited { color:#808080; } .exif img { height:1.5em; }' ;


/*************************
 * FILE EXTENTIONS INFOS *
 *************************/
$exts = array('gif'  => array('gd' => 'GIF Read Support', 'transparent' => TRUE ),
              'jpg'  => array('gd' => 'JPEG Support'    , 'transparent' => FALSE),
              'jpeg' => array('gd' => 'JPEG Support'    , 'transparent' => FALSE),
              'png'  => array('gd' => 'PNG Support'     , 'transparent' => TRUE ),
              'wbmp' => array('gd' => 'WBMP Support'    , 'transparent' => FALSE),
              //'webp' => array('gd' => 'WebP Support'    , 'transparent' => TRUE ), # gd support is bugged
              'xbm'  => array('gd' => 'XBM Support'     , 'transparent' => TRUE ),
              'xpm'  => array('gd' => 'XBM Support'     , 'transparent' => TRUE ));





/*******************
 * CACHE DIRECTORY *
 *******************/
if(! is_dir($cache_dir)) { mkdir($cache_dir) || die('Unable to create cache directory !'); }

/***************
 * UI ELEMENTS *
 ***************/
foreach(ui_svgz64s() as $name => $svgz64)
{
    $cache = ui_cache_path($name);
    if(! cache_is_valid($cache, __FILE__)) { file_put_contents($cache, gzdecode(base64_decode($svgz64))); }
}

/******************
 * PATH PARAMETER *
 ******************/
// path is mandatory, defaults to '.' if missing
if(array_key_exists('path', $_REQUEST)) { $path = rawurldecode($_REQUEST['path']); } else { $path = '.'; }
if(! (file_exists($path) && ($path == '.' || substr($path, 0, 2) == './') && strstr($path, '/.') === FALSE)) { echo 'Invalid path'; exit; }
$type = path_type($path);





/*****************
 * CACHE REQUEST *
 *****************/
if(array_key_exists('cache', $_REQUEST) && $type == 'image')
{
    // check size parameter
    $size = $_REQUEST['cache'];
    if(! array_key_exists($size, $sizes)) { echo 'Unknown cache size'; exit; }

    // create cache if needed
    $cache = image_cache_path($path, $size);
    if(! cache_is_valid($cache, $path)) { image_thumbnail($path, $cache, $size); }

    // redirect browser to the cache
    header('Location: ' . $cache);
    exit;
}


/*********************
 * DIRECTORY REQUEST *
 *********************/
else if($type == 'directory')
{
    html_top();

    // directory name as title
    echo '<h1>' . htmlentities(basename(getcwd()) . substr($path, 1)) . '</h1><br/>';

    // UP button
    if($path != '.') { echo '<div class="ui link up"><a href="' . encoded_self_url(dirname($path)) . '"><img src="' . encoded_path(ui_cache_path('up')) . '" alt="up"/></a></div>'; }

    // content of the directory
    echo '<div class="gallery">';
    foreach(scan($path) as $subpath)
    {
        $subtype = path_type($subpath);
        if     ($subtype == 'directory') { $link = encoded_self_url($subpath);  $img = encoded_path(ui_cache_path($subtype));                 }
        else if($subtype == 'image')     { $link = encoded_self_url($subpath);  $img = encoded_thumbnail_path_or_self_url($subpath, 'small'); }
        else if($subtype == 'svg')       { $link = encoded_self_url($subpath);  $img = encoded_path($subpath);                                }
        else if($subtype == 'video')     { $link = encoded_self_url($subpath);  $img = encoded_path(ui_cache_path($subtype));                 }
        else                             { $link = encoded_path($subpath);      $img = encoded_path(ui_cache_path($subtype));                 }

        echo '<a class="link" href="' . $link . '"><div class="wrapper"><div class="wrapped thumbnail_small">';
        echo '<img src="' . $img . '" alt="' . htmlentities(basename($subpath)) . '" />';
        echo '</div></div>' . htmlentities(basename($subpath)) . '</a>';
    }
    echo '</div>';

    html_bottom();
    exit;
}


/****************
 * FILE REQUEST *
 ****************/
else if($type == 'image' || $type == 'svg' || $type == 'video')
{
    html_top();

    // find previous and next file
    $prev = $next = NULL;
    foreach(              scan(dirname($path))  as $p) { if($p == $path) { break; } else if(in_array(path_type($p), array('image', 'svg', 'video'))) { $prev = $p; } }
    foreach(array_reverse(scan(dirname($path))) as $p) { if($p == $path) { break; } else if(in_array(path_type($p), array('image', 'svg', 'video'))) { $next = $p; } }

    // file name as title
    echo '<h1>' . htmlentities(basename($path)) . '</h1>';

    // up, prev and next buttons
    echo '<div class="ui link up"><a href="' . encoded_self_url(dirname($path)) . '"><img src="' . encoded_path(ui_cache_path('up')) . '" alt="up" /></a></div>';
    if($prev != NULL) { echo '<div class="ui link prev"><a href="' . encoded_self_url($prev) . '"><img src="' . encoded_path(ui_cache_path('prev')) . '" alt="prev"/></a></div>'; }
    if($next != NULL) { echo '<div class="ui link next"><a href="' . encoded_self_url($next) . '"><img src="' . encoded_path(ui_cache_path('next')) . '" alt="next"/></a></div>'; }

    // file preview
    if     ($type == 'image') { echo '<a href="' . encoded_path($path) . '"><img class="thumbnail_large" src="' . encoded_thumbnail_path_or_self_url($path, 'large') . '" alt="' . htmlentities(basename($path)) . '" /></a>'; }
    else if($type == 'svg')   { echo '<a href="' . encoded_path($path) . '"><img class="thumbnail_large" src="' . encoded_path($path) . '" alt="' . htmlentities(basename($path)) . '" /></a>'; }
    else if($type == 'video') { echo '<video controls><source src="' . encoded_path($path) . '" /></video>'; } 

    // file infos
    if($type == 'image' || $type == 'video')
    {        
        $exif = image_exif_infos($path); // PHP does not support reading exif infos from videos, but might in the future 
        if($exif != NULL)
        {
            echo '<table class="exif"><tr>';
            if(isset($exif['date'])) { echo '<td><img src="' . encoded_path(ui_cache_path('date')) . '" alt="date" /></td><td>' . $exif['date'] . '&nbsp;&nbsp;</td>'; }
            if(isset($exif['time'])) { echo '<td><img src="' . encoded_path(ui_cache_path('time')) . '" alt="time" /></td><td>' . $exif['time'] . '&nbsp;&nbsp;</td>'; }
            if(isset($exif['position'])) { echo '<td><img src="' . encoded_path(ui_cache_path('position')) . '" alt="position"/> ' .
                    '<a href="https://maps.google.com/maps?q=' . rawurlencode($exif['position']['decimal']) . '&amp;t=h" target="_blank">' . htmlentities($exif['position']['txt']) . '</a>&nbsp;&nbsp;</td>'; }
            if(isset($exif['altitude'])) { echo '<td><img src="' . encoded_path(ui_cache_path('altitude')) . '" alt="altitude" /> ' . $exif['altitude'] . 'm</td>'; }
            echo '</tr></table>';
        }
    }
    if($type == 'video') { echo '<div class="video_download"><a href="' . encoded_path($path) . '" download>Download</a></div>'; }

    
    html_bottom();
    exit;
}

else { echo 'Wrong parameters'; exit; }


































/*************
 * FUNCTIONS *
 *************/

function html_top()
{
    echo '<!doctype html><html>';
    echo '<head>';
    echo '  <link href="data:image/png;base64,' . ui_favicon64() . '" rel="icon" type="image/png" />';
    echo '  <meta http-equiv="content-type" content="text/html;charset=utf-8">';
    echo '  <title>' . htmlentities(basename(getcwd())) . '</title>';
    echo '  <style type="text/css">' . $GLOBALS['css'] . '</style>';
    echo '</head>';
    echo '<body><div class="wrapper"><div class="wrapped">';
}

function html_bottom()
{
    echo '</div></div></body>';
    echo '</html>';
}

function scan($dir)
{
    $paths = array();
    foreach(scandir($dir) as $f) { if(substr($f, 0, 1) != '.' && !($dir == '.' && $f == basename(__FILE__))) { $paths[] = $dir . '/' . $f; } } // skip all dot files and this script
    usort($paths, function($a, $b) { if(is_dir($a) && ! is_dir($b)) { return(-1); } else if(!is_dir($a) && is_dir($b)) { return(+1); } else { return(strcmp($a, $b)); } }); // directories first
    return($paths);
}

function path_type($path)
{
    global $exts;
    if(is_dir($path)) { return('directory'); }
    $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
    if(array_key_exists($ext, $exts))
    {
        if(! function_exists('gd_info')) { return('other'); }
        $gd = gd_info();
        if(array_key_exists($exts[$ext]['gd'], $gd) && $gd[$exts[$ext]['gd']]) { return('image'); } else { return('other'); }
    }
    else if($ext == 'svg' || $ext == 'svgz') { return('svg'); }
    else if($ext == 'mp4' || $ext == 'ogg' || $ext == 'webm') { return('video'); }
    else { return('other'); }
}

function cache_is_valid($cache_path, $path) { return(file_exists($cache_path) && filectime($cache_path) > max(filectime($path), filectime(__FILE__))); }

function ui_cache_path($name) { return($GLOBALS['cache_dir'] . '/ui_' . $name . '.svg'); }

function image_cache_path($path, $size)
{
    if(image_is_transparent($path)) { $ext = 'png'; } else { $ext = 'jpg'; }
    return($GLOBALS['cache_dir'] . '/' . bin2hex($path) . '_' . $size . '.' . $ext);
}

function image_is_transparent($path) { return($GLOBALS['exts'][strtolower(pathinfo($path, PATHINFO_EXTENSION))]['transparent']); }

function image_thumbnail($path, $cache_path, $size)
{
    global $sizes;

    $image = imagecreatefromstring(file_get_contents($path));
    if($image === FALSE) { return; } 

    // exif rotation
    $exif_infos = image_exif_infos($path);
    if($exif_infos !== NULL && isset($exif_infos['orientation'])) { $image = imagerotate($image, array_values([0, 0, 0, 180, 0, 0, -90, 0, 90])[$exif_infos['orientation']], 0); }

    $image_width = imagesx($image);
    $image_height = imagesy($image);
    $image_ratio = $image_width/$image_height;

    // compute thumbnail size but don't upscale
    if($image_width <= $sizes[$size]['width'] && $image_height <= $sizes[$size]['height'])
    {
        $cache_width = $image_width;
        $cache_height = $image_height;
    }
    else
    {
        if($image_ratio > $sizes[$size]['width']/$sizes[$size]['height'])
        {
            $cache_width = $sizes[$size]['width'];
            $cache_height = $cache_width / $image_ratio;
        }
        else
        {
            $cache_height = $sizes[$size]['height'];
            $cache_width = $cache_height * $image_ratio;
        }
    }

    $cache = imagecreatetruecolor($cache_width, $cache_height);
    if(image_is_transparent($path)) { imagealphablending($cache, FALSE); imagesavealpha($cache, TRUE); }

    imagecopyresampled($cache, $image, 0, 0, 0, 0, $cache_width, $cache_height, $image_width, $image_height);
    imagedestroy($image);

    if(image_is_transparent($path)) { imagepng($cache, $cache_path); } else { imagejpeg($cache, $cache_path); }

    imagedestroy($cache);
}

function image_exif_infos($path)
{
    $infos = array();

    // read exif data
    if(! function_exists('exif_read_data')) { return(NULL); }
    $exif = @exif_read_data($path, NULL, TRUE);
    if($exif === FALSE) { return(NULL); }

    // rotation
    if(isset($exif['IFD0']['Orientation'])) { $infos['orientation'] = $exif['IFD0']['Orientation']; } 
    
    // date, time, position, altitude
    if(isset($exif['EXIF']['CreateDate']) && preg_match('/^(\d+):(\d+):(\d+) (\d+):(\d+):(\d+)$/', $exif['EXIF']['CreateDate'], $m))
    {
        $infos['date'] = $m[1] . '-' . $m[2] . '-' . $m[3];
        $infos['time'] = $m[4] . ':' . $m[5];
    }
    if(isset($exif['EXIF']['DateTimeOriginal']) && preg_match('/^(\d+):(\d+):(\d+) (\d+):(\d+):(\d+)$/', $exif['EXIF']['DateTimeOriginal'], $m))
    {
        $infos['date'] = $m[1] . '-' . $m[2] . '-' . $m[3];
        $infos['time'] = $m[4] . ':' . $m[5];
    }
    if(isset($exif['GPS']['GPSLatitude'], $exif['GPS']['GPSLongitude']))
    {
        $lat = gpscalc($exif['GPS']['GPSLatitude'][0]) + gpscalc($exif['GPS']['GPSLatitude'][1])/60 + gpscalc($exif['GPS']['GPSLatitude'][2])/3600;
        $lat_degrees = intval($lat);
        $lat_minutes = intval(($lat - $lat_degrees)*60);
        $lat_seconds = intval((($lat - $lat_degrees)*60 - $lat_minutes)*60);

        $lon = gpscalc($exif['GPS']['GPSLongitude'][0]) + gpscalc($exif['GPS']['GPSLongitude'][1])/60 + gpscalc($exif['GPS']['GPSLongitude'][2])/3600;
        $lon_degrees = intval($lon);
        $lon_minutes = intval(($lon - $lon_degrees)*60);
        $lon_seconds = intval((($lon - $lon_degrees)*60 - $lon_minutes)*60);
        
        $infos['position']['txt']  = $lat_degrees . "\xC2\xB0" . $lat_minutes . '\'' . $lat_seconds . '" ' . $exif['GPS']['GPSLatitudeRef'] . ', ' .
                                     $lon_degrees . "\xC2\xB0" . $lon_minutes . '\'' . $lon_seconds . '" ' . $exif['GPS']['GPSLongitudeRef'];
        $infos['position']['decimal'] = $lat . $exif['GPS']['GPSLatitudeRef'] . ',' . $lon . $exif['GPS']['GPSLongitudeRef'];
    }
    if(isset($exif['GPS']['GPSAltitude'])) { $infos['altitude'] = intval(gpscalc($exif['GPS']['GPSAltitude'])); }

    return($infos);
}

function gpscalc($in) { if(preg_match('/^(\d+)\/(\d+)$/', $in, $m)) { $in = $m[1]/$m[2]; }; return($in); } // exif values are sometimes strings like "42758/10000", they need to be computed to an actual number

function encoded_path($path) { return(implode('/', array_map('rawurlencode', explode('/', $path)))); }

function encoded_self_url($path, $size = NULL) { $url = rawurlencode(basename(__FILE__)) . '?path=' . rawurlencode($path); if($size !== NULL) { $url .= '&amp;cache=' . $size; } return($url); }

function encoded_thumbnail_path_or_self_url($path, $size)
{
    $cache = image_cache_path($path, $size);
    if(cache_is_valid($cache, $path)) { return(encoded_path($cache)); }
    return(encoded_self_url($path, $size));
}



/**************************
 * HARD CODED UI ELEMENTS *
 **************************/
// each element is a base64 encoded, gzipped svg
function ui_svgz64s()
{
    return(array('altitude' => 'H4sIAOKRDlcCA7WSW2sbMRSE/8pUfT6Wji4rqdgOxA6l0EtI3ZY+Ft9iWNshce3gX9+RC4VAH/pSltWiI813RrMaXj1vexyXj0+b/W5kdOAMlrv5frHZrUfm52ElxVyNh6+mnyaz77c3eDqucfvl+v27CYxY+y1MrJ3Opvj89S0otvbmo4G5Pxwe3lh7Op0GpzDYP67t7M5659Te3UyEe6VNXHWxiS2h6gaLw8KMh60BLe2eRn+hUOXaboPjZnm63j+PjIOD5sghhmpenmQ8fPhxuMdq0/cj8zqt2mOwGJmtulihNYovASkFfpNECZqRciFLi5TzB3Upo3O9BFfRJS/aXspSyCidQjvPztL5IBq0/n+BsS8PtVr9OdEF9K+c89YhdWWuLT+uJPgOGhAcQkEioyIrckAHj4gAdaLoJILRMKbgJXJRVC+FQmUb1TdeRJsz4N6TkHJtfbyTglgZMmLpRX1Iom4uTSK0lcVXkoVMJ0kIUQmx9zFIqG6uemlbpLn1Qlsenigapg86j5cKUpRMs04incXmL2RWYkbuud9XlOa3m6eL98yUWGzU394T2KUdCrwhzCS3Es1QSAqoSW2RgSQJiDRKRSfZMYx64XF7G8/tX7XbOv4FU3JIu2UDAAA=',

                 'date' => 'H4sIAPySDlcCA81W227bRhT8lS39kgDd5d4vheUAsY2gQNMEid2ij7JEWUJpyqBky83Xd4ZU4AscGEXyUBCml8tzmzlzljp8c3fVitum36zW3aQySlei6Wbr+aq7nFQ324XM1Zujw59OPhyf/fXxVGxuL8XH87e//XosKlnXf7rjuj45OxGf/3gn4FzXp79Xolput9e/1PVut1M7p9b9ZX32qbZam/rT6bGEreSDLtrTuUZQo9V8O6+ODpkAJXWbyTNR4KVpXT2p+HbV7N6u7yaVFloYX4IywpjsVUbEebPYHB22q66Z9u/66XzVdFuxmk+qi0r8YyeV0zATl/s3591qi9w3m6b/fD2dNR+6801TiTsYeqvsveFZP+02i3V/Namuptt+dffKeWVj/lnj2i+lsUn5gv9Bq/Aa+czXfHdYxaG+zXZ9LXiTs3W77ifVQWN5VWK9WGyaLVBV9XNmZcHr3szQrH4M9Fng0xG4zcr/GOB4X4og+fulNA49sEJm5Ah2j9x4JiRy47B6FnpqLpqLF6E38YmZCs8bLhYvElSPArmebpcC5FxZbVUR2Wdll0Zrp+IMTAWAC0Z5Ya1y4yro1nsqLRTlZ1qg10laOgct9xZLaXRKyszkGGPYp5EbV4hh4S33MSRjMEXBG7G3+PJ+LCklz5cvryuxWLWt7G/aZlI1t023ns8r8NKv/8bGQSy8vm7I3Wq+XUKWXrnRESazwIt0PaFliP8/oeW7Yd707auDi9ePYZrIVCGpCBEXFJe0AbTsIfCZ13GA7ItBLQY12ofrDWBB8NJlZVC7Z4yCjVbiXqRLDuTJ4pPKaKqPYMnmokwLdxiICEf/31/8GCKmT4i4T/Yw1yYEgAAFKXNPz1y0KpKSENDnYuF0v9zIAjpl1MqCzUwhgEFUD+cgnUtgJWMdJVr/5UHNw9x+u+Bu3TWPq0UKJwZJ2cJ+aRQVsmUH0KOcPHaK8LQCCgFw0CG/EziZjSBKYblv4BE5/5FatIMvy0MDnQRyGQqSMJjM+M4MhUtPNNSrDwCakFuiWk+RIyYjOU35ehkTqoJNBPLvF/DBIupHNARgByTAKTNWIIaZddCsCQQJBpAdD2ghTmgnOI4pS+u51vhgShz2SXiHlUGrQCLLNzFwGdg0D+gaUBJvxGs0OTBF5XaAVnA0+CEDxWCGkXFlfyJEYR1vSC4Tp4ejgtlxbAnGm+SiY9I6SIXEcZ7QtsTCMFn0o44Shy4MVVJEKJL9g4/wtIU+ETdqJoYERDZEhT/8LGCxDlRYStMgzmg0CAEoigRhjDSeSEYGkmEpDEQJBBp5IGUBgWDEBYIBS2YFKKUdykHoWbD08kg5hKIHeUTJwEz6KY00tgcMJJG1iLQi134YGY5gtqMS2SBNb1KbsXDY8cNRyP6C+vfjuSxS4XH5zYdHinn4YilzbMfDy1lSjt4R09CpYYiRhgR5SgunmANzsB4+q/hZdvQvfDd0fk4KAAA=',

                 'directory' => 'H4sIALuTDlcCA9WUW2+bMBTHv4rnvjQPxncuU0ilJlE1aVqrNtm0RwImIBGIDIF0n37HJFW7Klr3Oh3Lxvb/XH62xfTmuKtQb2xbNnWMuccwMnXaZGW9jfGhy0mIb2bTT4v7+ernwxK1/RY9rG+/fpkjTCj9IeeULlYL9PT9DoEzpctvGOGi6/afKR2GwRuk19gtXT1SwRinj8s5AS1xExYx5ZwpBOXMy7oMz6YuAZRUt/GFKODFnBq/q7gvzXDbHGPMEEPcDzXiMhAQLTN5O5tWZW0Se2eTrDR1h8osxhuMnkWMfQXe2/PGui47SHtojX3aJ6m5r9etwegIOqH0q25lk7rNG7uL8S7pbHm8Vh7XCrnkypPC54jwQHkqglH52gsnkI3HWHEfwvFTOEDtmj1yHUmbqrExvtpkzjBq8rw1HeBgekmmlbNXGXcy+iflRerkRK0D/m/UMvw7tfDDF+zxE7AFHACMMgq96Iwt1Qu29C9jC2cfYsvE2UfY9HTn+6QrUF5WFTgGzBlGcAI7oULks5SoADECD0WGrg91H0RRCiABrKBxBVrBeSRSJ4UJGTdA3RMNqwxCkNH71AqiGcSVgmiiBBERUYqosCeBvKT1xS9XvjVphyy8XA0PzD7DCOdQmHJbAGDkih7KrCsAVvugeHanCZcHDj7snfkUd+bCvaE+2Or6Kpm8oY7+Z2op31GPfJvJ+AbglzD7Dfflo67KBAAA',

                 'next' => 'H4sIAGWUDlcCA6VUbW+bMBD+K577pZXqd4NhCqnUtKombWvVppv2kYJJ2AhEQEi6X78zeWm7pWmlCQHnk+/ueZ7zeXC2mhWos3WTV2WEBeUY2TKp0rycRHjRZiTAZ8PBh4vr0fjHzSVqugm6uT///GmEMGHsuxoxdjG+QHffrhAEM3b5FSM8bdv5R8aWyyVdKlrVEza+ZZJzwW4vRwT2ErfgIdcumEFSwWnapng4cAUAUtlEe7JAFHe78V+Iu9wuz6tVhDniSBgpqI+EpxXkS23WDAd1nOZxceV+tmxRnkb4AaPJZn1f5i3UWzS2vpvHib0u7xuLUfIYYUlDMCCzCKl5ihjXcdlkVT2L8Cxu63x1LCnXvjnl8AjqecY/JRDiieBUUe1JcYJRDVkMlY5kW82R+5CkKirwH2VZhlGVZY1tgQVmr+7pPRWgzNtHt3MXJFwQe0l0L/H4XcSVT/WaudQ0OMRcIKc69ZRvpJPfp4FRG7oe9bd0XyH3X1SS91GRjkBPRVJ1iAoNtOKy5wPnSoKpqQ5DBSsVyG0L9zbQqFQK/80eahvG3H+LKluf2glq/4GoOZW+10PcmCRQ1AsFgpnyAeYJwJvH7RQ9KetIg1ozCPCQa2zsGoMCqh1RJIgCfs584UVr7+8vQT9NELo1MMryoiD1orARtp0tqzTFe7AKKo2Qm5noTRJQIUINs+FRwZ2iLhO0rS6Oj+ITJ0aPvWnr6pclRV7an1UOY15XizJds9ii6ISZCtUF1CskJ3DsQlCACiIM+HRBBMwuwN8Pdl0AGqJio7jGzysm8XxXcOOe5a2tixx+0DL+AnXyhPqZ4t4aq5QgmuwMDaYOT9CFCVwBAZJuTHxiXD8E1UQAXODlZAlfQfys5MOhknDp9Mk74Tl5DEgBJT0iYIzJzgK/hJqqF2gnxuaGOUC5rErbH9kJvHATD/8Ad6QES0EGAAA=',
                 
                 'other' => 'H4sIABiVDlcCA71WbW/bOAz+K5r6Zf0gWZRkSzo0HbC2GA64vWBrb7iPri0nxlK7cNwk668f6bht2suWojccDAS0RVJ8Hj6icvRmfTVny9gt6raZcJCKs9gUbVk30wm/6Svh+Zvjo1enH0/O//l0xhbLKft08favP08YF0ny1Zwkyen5Kfvy9zuGwUly9oEzPuv76z+SZLVayZWRbTdNzj8nWilIPp+dCPQV9KKCshScYFJQsuxLfnxEG2BJzWKyIwtGKfLmType1nH1tl1PuGKKgQMvLYOQBekwYxmrxfHRvG5i3r3r8rKOTc/qcsJLzr7rCTdeAmfTceWiqXvc+2YRuy/XeRE/NheLyNmaHK3UD47nXd4sqra7mvCrvO/q9WsbZLCBUQ2jKYwBmQITOs2kyw5xQ8A8mQyYkSwjLWHu22tGP6Jo52034QdVVXHWVtUi9giKJz/1Gb60WGjdfyfP+yCgoOQx6p0sFCMLTvrnsWCQ1ReygFQrM7JglUxHFrKdHLiCnr00XHp6XgD8cgQeqI7nAE+3Gfo38NRIo7I74BYsExaQAUPAHTIxAoex+VpLs7v5qlJxf/+jp2fLTWa/EMqDl9/tVXp8zAt4zO95DM/jca989GP1oFKUeaIenZEK1xsL/tczlGwGynXezxjCvwIwMmM+nYENIE0BqBMsX3uZMtC4NFj4Y5ZgcA6EAmeUxlOEyCgSlYK2zbQMG3smQA1+YpNKUAJBqTbWkEqAw2hbKHJytJEbFti9yy1nVT2fi+5mHic8LmPTliWh7dpv+OHAe5/79O6DWNVlP8NjObSRArF73fz1weUhsdDFomfdeqCn27A0i/V0Riw5pVDJbEwA1uL4fdhnSJJT23ANRyipn6yUpu7P927aJtLGWzTjPA8MvE8LUEATPkNerQtCowYMs5mRXlhtEb/DmY6cwHDyHMusNMJaYthpuZEXHk3HjHa4ovFQMJPiebx9DxrdDF4iLh36t/W2j88M+bR7+SwO72E9aI4IG0Bu71dkSKSgW4dpDVKjLBzZHhWPNdMXN4CwAiyt4CVKrbeIBofagHLgITUCKCvK090+6Ux5+JwuDO3fKlfv5uJOEmFIcScIhYIIQ/9TAApd0zCly/k/Zg7B48yjxA7g96X1wY31BqBx+dsSbzEBADSvH6WmfzbHPwCdk4BLkQkAAA==',

                 'position' => 'H4sIANGVDlcCA8VYXW/bRhD8K1fmJX048r4/CtsB4gRBgbYJEqdFHxWKsgXYkiEpsZNf35kl3bqB2gqBUb8oJO+4uzM7O0fn6Nnt1aX6NGy2y/XquLGtadSw6tfz5er8uPm4W+jSPDs5+u7F69Oz39+8VNtP5+rN++c//XiqGt11v/nTrntx9kK9+/WVwstd9/KXRjUXu931D113c3PT3vh2vTnvzt52zhjbvX15qrFX88ZUE/hyh6DWtPPdvDk5YgKUtNoe74mCtwx3N19VfDEszy92uMkmtKlRN8v57gK3qdbWYfNyuHm+vj1ujDJqfKimrSdH82GxPTm6XK6G2ebVZjZfDqudWs6RvlGfHYLE1NpGnU9L71fLHWr7uB02765n/fB69X47NOoWO725t+9sM1ttF+vN1XFzNdttlrdPYxtMjoo1TJcAbaxXpQ0x5u+Rzv6Z7haXrB2E7NbXij+6X1+uN8fNExfTUBeNWi8W2wGoTdPt22aMH2L/1zbLbd3fge4Ffj4BD7GNhwJv/QNAHxMSunX/BD6Z+sEN/wl+NvTFlnvb0OsD4S8m+JVKOgC9c7nND4Be8gn4WCGBxwE/jOBD4twcAt74+yL5VvBjPgFfHQI+Dvj5CN75chj24NoHmHhJR+jO2jY8EvT+zuzC/Un+F/DZt+Uh3E7ySeMNpuhxwH+Y+h4PFP1XzvjNjY93ok/l0TQ/m7CXeuApZ51v6wOAHxMSfU4I+D+g78aj/nq2u1CL5eUl8G0unz65QEUg4somH1WJrk9tUj5U7UJVqdi26lgKPhlyQsO0N4ivimm9TjCAqH3GZXGpDZqggra14IlLhk8w0xkbI59EXW3EyxaBCm5NG1VAAq99RfgMgrQPBqm8QQisqZhDG5SrAQ9TYOaIz5eqrG8zLpXD1CjLs4OXCI3C7JefbTD4jMB7Eb/3b0jM1wSc3xFwb1+vocmskaZq6xxKzK5NgIaW6lSxFCrvE4Ak/KjAPQ5gFXBjAeCVddgXHa5An0a1ngRiZ+HOoAEYOyNTgNSCQrFSGRJselypkrUncmxUyeOVKPwht9M2M0VyXOcm1BqUr4hD5hUaxEfYB5dGL7Uv/NfoghNrXHGM4MkgWlEV+VeZxFrDFcf+YEixO/HHOFaRQVGyuC+JzZcme1RLU8ikxiIjJcR2ADAyeCmUeVC4V2NTLPuOjmZyAbCZL4KMCNIJD5GiZnbtHUVlKbMUR+ZBExNF0RFlYw3bFRkOX7ZkO7NRnjECWXYWmiYb0FtQ0bJv/A6WklFTkqY5kRbVxHSAqBKhWCTyQnsVheExu58nLQc2iLt570gQCIMQIqmUj23PniM/gmQORyDLmbwIvZGAg3DOjynNgxBMYxrRTVE4JBgVJg0TQX14I48qhw85vRUKIqVAhmyRqJY8FJYTAc4zmK3sP7EYTCXhjC9ExrfUsKceMynngsRIMqIFeREtUtFoVWJrHDUvrzJNYotEd2Afo00mHLqL6eAyqxR+YChpHFoUnaiAQCg0k1EWWQaHo0aUrH+0HLxlqgw8BWgtx9XCG4pCdVmJEzkjhEsXOAw2crS8zC3HU4bZBrnM4k8QHwY7Ayj/MgLQEZkTqPXLPstYTJZRpOcg0vbWCV+EHNiIgH7hdJe5ETFznjgbKF1nS+zoROLw5HEdHKIQz8nMWda5VXRPvFre57wFmWBqPdm95Q13jmYq+gfIqeeUwSPRQ7AG2XFkPY0LMsv813I9UFykRHlxtrw3/PwuPBFwYCwJcGJyTsZAJ2mbJ7nomwwhVJA0Fp1CUSDCjxbJE2NUH73WcbZFYHAkDdns57+/qwAFWGoKOXtN/koaXRKcZQK1Y3Z2hJxmTx3BPoJAhsA8Xc+y+6i20iTENwo9AgcWZ7jQISgrHv26MA4NibqirCLbmBOHAwt0Fmky7VxcVwYoj7PjBbqZzo7IYeQBU8bDI8qUWiW26OXgoAqzsAyVeQoDAoOu8ogsVxE/oBhq3NMyMCp0F3qZEwF6OTQM37Siexp7lBLF16ThIYgPhX10f5jozlXkjvA9Dw9IOlMlMnu0jcK6ipxv9Fp+I4ii+IeCFuMT32VlcvQFw65HaVVSmQSAf6vL5AwghUTk8RDg3JPqxI65iWh+GPhMywQTe7Uym4oPTvw4YmPoM61cTicr5yE+fRxLVXIOJXE4sR8eKz6Jp0onHc9NArOCzrBtZWw9Sh/Vyv+iOfkDHeLrsloSAAA=',
                 
                 'prev' => 'H4sIAK6aDlcCA6VUbU/bMBD+K575AhJ2/JqXqSkSBaFJ20BQNu1jSJw2WxpXSZqW/fqd0xdgKwVpipKcT76753nO58HZalaiztRNYasYc8owMlVqs6KaxHjR5iTEZ8PBh4vr0fjHzSVqugm6uT///GmEMPG873LkeRfjC3T37QpBsOddfsUIT9t2/tHzlsslXUpq64k3vvUEY9y7vRwR2EvcgkVMuWAPknJGszbDw4ErAJCqJt6TBaKY243/QtwVZnluVzFmiCEeCE59xLWSkC8zeTMc1ElWJOWV+5mqRUUW4weMJpv1fVW0UG/RmPpunqTmurpvDEbpY4wFjcCAzDyiwVPEuE6qJrf1LMazpK2L1bGgTPnBKYOHU60D/5RAiObhqaRKC36CUQ1ZAiocydbOkfuQ1JYW/Ed5nmNk87wxLbDA3qt7eo8FlEX76HbugrgL8l4S3Us8eRdx6VO1Zi4UDQ8x58ipTrX0A+Hk92kYyA1dTf0t3VfI/ReV9H1UhCPQUxFUHqJCQyWZ6PnAuRJgKqqiSMJKhmLbwr0NDGQmuP9mD5WJEua/RdVbn9oJav+BSBSjwtc9xo3JQ6apQDBTPsA8AXjzpJ2iJ2UdaVBrBvs1co1NXGNQSJUjijiRwM+ZL7xo7f39JeynCUK3BkZ5UZakXpQmxqYzlc0yvAcrpyLgYjMTvUlCynmkYDY05cwp6jJB2+ry+Cg5cWL02Ju2tr8MKYvK/LQFjHltF1W2ZrFF0fFgymUXUl0KRuDYRaAA5YQH4FMl4TC7AH8/2HUBaIhMAskUfl4xTea7ghv3rGhNXRbwg5axF6jTJ9TPFNdrrEKAaKILaDh1eMIuSuEKCJFwY+KTwPWDU0U4wAVeTpboFcTPSj4cKgmXTp+849rJE4AUUFITDmNMdhb4BdSUvUA7MTY3zAHKla1Mf2Qn8MJNPPwDpSKlZkEGAAA=',
                 
                 'time' => 'H4sIABOYDlcCA61VYW+bMBT8K577pZVi7IdtMGlIpaXVVGnaqq7btI9RQhI2ChG4TbKp/3020JY0kC3rZCHZT/a9u/MhD87Wtwm6j/IiztIQg8MwitJJNo3TeYjv9IwofDYcvDn/OLr5dnWBivs5uvr89v3lCGFC6Vc+ovT85hx9+vIOmcOUXnzACC+0XvYpXa1Wzoo7WT6nN9fUZQzo9cWImL3ELljAhD1MDSgwZ6qneDiwDQyltAhbUMwpZnfjF4wXUTxf6BBLZRareKoX1dzA6U0SIb1ZRiHW0VrTSVHgoVOwX7M4SfpHitlx+uAUUFdms9lpofPsR0SSOI0m42U/z+7SabP4PYvT7WrZtA9QryuYhwEt+w8Hc6TzcVrMsvw2xLdjncfrYyKUAyLwgh4z42kBANwRCoIe4dzhzPOle4JRhRviI1/agelwkEcT3cQtJuMkOibQg5MnFwL/2R3uYrQOMZEAGG1C7PqmYEUb1MqH16CC90fU7tOefD676xTrme6lS+zkcGwB4hXgy7FeoKnZ7QbIVWrsegzZrxoSAlStqxogRsraT3tj5upDXKYqzdJoOyqcNaPyUoZkOzLAr/xV7SocyQLwXV/2mKM85nmCi+aUNHfs8bENWgmhZMA9gye5CJQSfnNKmjtK6I3Ng01Zydvd1tKQ+LcUSLs8slefJSHgMZ0QHMii+yYIr38gIlyG2+m2WUb2enYwC4/5tdOdLJ4cIR2u/XMomvxJh8a2WHi8Jg/81ano0LdPnmHAffV4i+r/RQI4q9El3v8Pkfaf6LA8uEI0OMiaAxM1B78rD6U5HnOl2Jo92jNJxoV5eAt42Q/83X6ukHX+fNauOTCPVwCe7SBckH7AVHNKmjt2+9uHfvgbl6gS3KAIAAA=',

                 'up' => 'H4sIAKiYDlcCA51U227bOBD9lVn2pQHCEe8UizgFmgTFAr2hTbroo2pRtnZlyZAUO83Xd2i7ddI6abAPNsnRXM45M+TJy5tFA6vYD3XXTphEwSC2066s29mEXY8Vz9nL05O/zt+fXX75cAHDagYfrl69+fsMGM+yf/RZlp1fnsOnz6+BgrPs4h0DNh/H5YssW6/XuNbY9bPs8mOmhJDZx4szTr48HUQQJgVnlFQKLMeSnZ6kAgSpHSYHslCUSN7sF8SrOq5fdTcTJkCA9DLHQIuSlK+M1XB60hdlXTSv0xLbEepywqYMZrvzVVuPVO96iP2nZTGN79urITKYfqOEaGhDmZVBa/Yhl33RDlXXLyZsUYx9ffNcHHOFwjh/LNFa747FsUJPR+cwaH3EoCe0HqVPLMduCemPT7umow/Pqqpi0FXVEEeqyrIHfTaWjmDWY8K3D5IpKLvP9CDzr09irhQat+UuJWr1KHfgmBstVJoBRYsAdMIZA0ahzd2OPAnhDpL3ulTS/ZG/iaEQ7n8wLp7EWDs06me380e7LSGNGlrtfGJL1HL/o8cWnf1B8wFGT+9fth3gGYy/YSBthXcbILstl5YGlhYTMJgjArEsxjnsq6FmQHosDLnlsCFcbPBCTnHEAyTXElVI+/t22Nlv30qDJoDO0TKo6qbh/XUTJyyuYtuVJTuAlCKlsTvJgjWE1KF0Ok/CaZmES4moLX3z/FlxlJTYIB/Gvvsv8qZu479dTde9767bcsthD2NO1yrkKy4VBjvPUeiGp5um6JlBYXnqjgVJmgQgTw9k9YkV+ZvbwyS2lWnodOG1MOwulGmx/IlkZ17UY+ybmhZqpLhH5+uezp1GWLll4UkQUJYwzz0a2WygObRuSvI7LmXqQwg8p4FMatnAU2N0EpOMKvBAD9N+16TPnoTN0ckHuN0BNz0IzuQ7iakSaJ8UJhBbgcOc1JRUxpDYSWZj0t4TUJ+Gir4qTizoWqDUG4397R09d4/YI6q1XRs3d2FGP3rtT78DltJOC6UGAAA=',

                 'video' => 'H4sICISNDVcCA3VpX3ZpZGVvLnN2ZwDVWNtu20YQfc9XbOmXGPAud3f2GkgJkAuCAkUTJE6LPsoUZQmRJUNifPv6niGlmokvkdMYRU3AXJq7O3POzJwdevDi4mQuzurVerZcDAujdCHqRbUczxbHw+JLM5GpePH8yeCX1+9eHf71/o1Ynx2L959e/vbrK1HIsvyTXpXl68PX4uMfbwVWl+Wb3wtRTJvm9FlZnp+fq3NSy9VxefihtFqb8sObVxJzJT/orB0vLrGp0WrcjAuYYgtwarEe3rINlmmeXnzj89msPn+5vBgWWmhhvDMqJWuECU6rYKzDxmIwridr3MVgPlvUo9Xb1Wg8qxeNmI2HxYhn4NW6WZ4K/iWr5Xy5GhZ7kzFfhVhOJuu6gYWivGvq2PF1PdV0Uwfl1wbbv/F4NP/Kh6NCHG+ePy1mDQj4sq5XH09HVf1u8WldF6K6HBbkVYiEMdDaqKwJ18sOV6PFerJcnQyLk1Gzml08zZiR7IHGFa3SkQ4kVhjlDqSJxijaLwRcN1n5aO7kwI/52okD524j4Gu0dwUBCC8tViUVMn2XjItuaox0HwFEKjorOC82Q2l8JJWFdaSSBf5LMyyCMkZjowuMrYrImY3rt7g5fjQ32zhthpJUCDEfOK98+gEvq42XRhkdd/IyqmxTeJiXpIHMHfgMLvLGS+OxUe68RF0DxJ2JlYmv3Yor87VLcQ3KTaUPjkVzjaEdzkdN/VT6CFKsAJvW6f2iK8i6amDxcl6DO7b5bE+3P4UYz9an8xFq72i+rD6jYFB6lJQPSPXV5fV4Ws+Op3AtO6diKsT5bNxM4alNCMC6WS0/Y+89c8QXqEI8kRgJC7mUDXLSb6fJzdKO5UJMZvM5graaP9072t8Abx1eIoazBlspMsaH4gEIsrLkc4dgO94iSDErsj0EBjrSw5BGKXCqMAaXVTIdBheUC/djWCwXdR/A7f42N1JPqoyfKJTW0Zmse4PuBSp8/55QUS9UdA3UQvlCDyhBFEwPqD3iqwXqM175FigkxADqDtGqt9E6HTXT9oVcfWHA9Vm9WI4hJSjVE5h1ioTPCfwJQ6hZ3HJQIUmcYUgm6RVpChK5pDJJkxCPjPPNAo50SqdornaypG0SQeMo8MJYFCrMeOUczEBvnES5wgYsZgluVfSdDWIbeScLAW57gg0gMq0NYizgrrMR2YZpjViYTYDc2fA3beyWzGzV4qDLbDWo7CqZYJtIJGU1RQkN1I6E1UpbKyFJ1jkBkomcwN/ANSLnLY8JXUOImM4PLgGA1qAYMVGG4WRkG3YKygYShKcU+UbZIXp4Et5BVZhHCg42jYYPhMdoU+rIJKAGUKJ5SwJIb5nOlbR4yXa1lciH0A6DDEGlnLuHq0csnU0uXyuKj97Q7SWFJHQUupLajrclZTwSyve0IyImvZKaTCadcIBFbTf15BIO45+sHKtl06q9scBp9h+u45aQo31x8AhQXxxqvlowlNFy5hYM0gFZvYM2jHfSBkKzgrMKuQptQI5EpHYQ3MHoKJFtAXluXIQGy2QQCjQ21iDdhOd1SH6s85yIYNtLlg8ufSSrThJiRz4J3lNnGZGFmjiDg+F16BKN4D1tQNNokdeS92RpMsp61DDvGWBPuQS94D1D12Wl3aQiBdgXFhazBbQMtwENzRJJbOY8Cg5HtQsygdMYhSV0QLlDRhzX5P0/yDy8zx2yKLtQim5LGXHeW9ZX3hOOIqxedHtKy0IQZbdni8zlDplvkbHatnt2yNyOMouNojDJ4nSZS+CM7A0o1bZ6gPuIljWyZUSAieTYGWZEdFvKjpHOe9N534+L17nvPbkflFfjoFRGZMBJqYKCEhQJIgg9hcvKITboEHB+tTekDYjXjqckH9oEc0AAEY3GcYJlIIAc+yjAEKSIyTBQ4FaUrYQxnHYCYhkCSUB0IApHSM4eookPGYgvpyeiyI1KAn0QE+8rtKM4q0BNhKzhMwfyCaMeAo769EhSGCVKV/9ORx6slLGnlLGnlMGrRP0+Md2qlEx+7NrEiLTKuwtlG+b2fPRgVjj+YPRnFsRGU/GXMghuDzcLikAexLpLum0ophbHD9kK8TTtZ5QDCclJ3HzMHEOXcER5ZBhCY3XmlsXimyX5ShImYWqU2xx3ymxLC/1TilfFrRV0o1X+rpRW+w/P6255R3IrtMQ0CIf0cnZk+IjnDqu9tf9WEC0g4vPy27coPbF9efXTXAmev67+W1e8Jm5Gf64r15VjA9qqDeiQWNzxGYNCn/I3VDLfFsND0v5eIxZqEh/LCD4rI3e1Bq2AO4MCuZtl/f9AEnFIQkkfGQkOKGg/9CWpMHWO/1nyQ0YG5fHzJwP+/+DzJ38DmG3LgNoUAAA='));

}

// the favicon is just the "directory" SVG converted to a 64x64 PNG
function ui_favicon64()
{
    return('iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAACM0lEQVR42u2ZPa/SUBjHn97eAhUI3JSBkHRxIOFTGL6Di3EwzJddbmKEhMSkToQYEwfWuzo5YW78Dg46yXJhgmBKgNKeFxdJsFhF5Erl/H9JFw4v7a/P2ylEAAAAAFAV7VeL9Xq9VCwWX5imWZFS8kP/uOd5X2azmeM4zsfYCWg0GvfL5fIny7IS+3yxlHKn97muKwaDwcNms/n2GALOoxZs277O5/MJzvleF7Yr6XT6rFQq9YjoKALOohZM06wwxih8cM4PfmQymYtWq5WIVQQQkRG++/uyS9Tkcrl7ROTHRsD6bh8iz+NMpADOOTHG/tmJeJ6XJ6KvsakBQog7yfe12J/UljfHqAORbbDb7S5SqZR5F60v6rOu6/pCiMXfXpQQgjRNk0EQ3M7n8yvHcd79sYBOp7NIJpNmnHJ9n/MIgoAmk8njdrt9/VsB/X6/slwuq0SUmE6nL3VdN05h3PV9f1YoFJ4TkZ/NZm+q1ernLQG9Xu/Stu1Xmqad9OwvpaThcHhZq9Veh7vAUyGEEhsgIUSDiH4UIKU0DzX4/AdRYG7NAesWpQKb13ke7vuKpAAEIAXCAqSUpEoX2ByoEAGoARAAAagBiIDvLyq0GUIEQAAEhATs8j/AqbD5uB8RAAHYDWISRAogAhABEAAByu8GUQSRAhCAQUjpGsAY47quq/I8gG8JCILgxjCMRyoIWK1W77cEjEajJ5Zl3RqG8YCITjUUOGPsw3g8fkYAAAAAUJ5vGcp1jDoXRO4AAAAASUVORK5CYII=');
}
?>
